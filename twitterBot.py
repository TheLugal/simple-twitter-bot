#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

from time import sleep
import tweepy
import conf
import datetime

# TODO move configuration to YAML
# TODO write actions to log file
# TODO use lists to grant command-access (retain irrevokable master)
# MAYBEDO use screen_name instead of ID. (usability, and such)

auth = tweepy.OAuthHandler(conf.CONSUMER_KEY, conf.CONSUMER_SECRET)
auth.set_access_token(conf.ACCESS_KEY, conf.ACCESS_SECRET)
api = tweepy.API(auth)

master_id = 161045949
my_id = 2593489249
timer = 900
saved_searches = api.saved_searches()
rate_limit = api.rate_limit_status()



def logToFile(log):
    now = datetime.datetime.now()
    #stamp = now.strftime("%Y-%m-%d %H:%M").encode('utf-8')
    stamp = "Not too long ago; "
    f = open("actions.log","a")
    f.write(stamp + "+ ; " + log + "\n")
    f.close()

def messageToMaster(case):
    # TODO Predefine different cases where master should be DMd.
    #Someone tried to send a command: (command)
    #could not fulfill request; reason
    pass

print("Running...")
logToFile("Started running")
#print(rate_limit)

# def parseDM():
dms = api.direct_messages()
for direct_message in dms:
    #print message.id
    #print message.text
    sender = direct_message.sender.id
    message = direct_message.text.split(': ', 1)
    command = message[0].lower()
    argument = message[1]

    if (sender == master_id):
        #def tweet():
        if (len(argument) <= 133):
            api.update_status(argument)
            logToFile("Tweeted: " + argument)
        else:
            messageToMaster(tooLong)
    else:
        messageToMaster(attemtToAccess)

    api.destroy_direct_message(direct_message.id)

        #commands = {"tweet": tweet,
                    # "follow" : follow,
                    # "unfollow" : unfollow,
                    # "save" : save,
                    # "unsave" : unsave,
                    # "follow" : follow,
                    # "unfollow" : unfollow,
                    # "save" : save,
                    # "unsave" : unsave,
        #}




#while True:
#    favorites = api.favorites()
#    lastFav = favorites[0].id

#    for search in saved_searches:
#        print "Searching for " + search.query
#        search_query = api.search(search.query)
#        for tweet in search_query:
#            if tweet.id > lastFav:
#                if tweet.user.following is True:
#                    api.retweet(tweet.id)
#                    api.create_favorite(tweet.id)

##                   twid = str(tweet.id)
##                   print "Faved and RTd " + twid
#                    logToFile("Faved and RTs " + str(tweet.id)
#                else:
#                   api.create_favorite(tweet.id)

#                    twid = str(tweet.id)
#                    print "Faved " + twid
#            else:
#                print("No new tweets found.")
#                break

#    sleep(timer)
